﻿namespace WeatherAppBackend.ExtensionFunctions
{
    public static class DateTimeFunctions
    {
        /// <summary>
        /// Convert datetime to yyyy-MM-dd format
        /// </summary>
        /// <param name="dateTime"></param>
        /// <returns></returns>
        public static string ConvertToCustomFormat(this DateTime dateTime)
        {
            return dateTime.ToString("yyyy-MM-dd");
        }
    }
}

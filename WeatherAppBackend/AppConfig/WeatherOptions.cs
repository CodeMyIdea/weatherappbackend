﻿namespace WeatherAppBackend.AppConfig
{
    public class WeatherOptions
    {
        public int FrequencyInMinutes { get; set; }
        public List<string> Countries { get; set; }
        public string Url { get; set; }
        public string ApiKey { get; set; }
        public string Path { get; set; }
    }
}

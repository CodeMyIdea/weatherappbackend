﻿using WeatherAppBackend.Services.Model;

namespace WeatherAppBackend.Services
{
    public interface IWeatherService
    {
        public Task<WeatherHistoryApiResponse> GetWeatherHistory(string country, CancellationToken cancellationToken);
    }
}

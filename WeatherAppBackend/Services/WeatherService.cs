﻿using Microsoft.Extensions.Options;
using System.Text.Json;
using WeatherAppBackend.AppConfig;
using WeatherAppBackend.ExtensionFunctions;
using WeatherAppBackend.Services.Model;

namespace WeatherAppBackend.Services
{
    public class WeatherService : IWeatherService
    {
        private HttpClient _httpClient;
        private readonly WeatherOptions _weatherOptions;
        public WeatherService(HttpClient httpClient, IOptions<WeatherOptions> weatherOptions)
        {
            _httpClient = httpClient;
            _weatherOptions = weatherOptions.Value;
        }

        public async Task<WeatherHistoryApiResponse> GetWeatherHistory(string country, CancellationToken cancellationToken)
        {
            DateTime currentDate = DateTime.Now;
            string pastDate = currentDate.AddDays(-7).ConvertToCustomFormat();
            string todayDate = currentDate.ConvertToCustomFormat();

            string apiUrl = $"{_weatherOptions.Url}/history.json?q={country}&dt={pastDate}&end_dt={todayDate}&key={_weatherOptions.ApiKey}";

            HttpResponseMessage response = await _httpClient.GetAsync(apiUrl, cancellationToken);
            response.EnsureSuccessStatusCode();

            string responseBody = await response.Content.ReadAsStringAsync(cancellationToken);

            return JsonSerializer.Deserialize<WeatherHistoryApiResponse>(responseBody);
        }
    }
}

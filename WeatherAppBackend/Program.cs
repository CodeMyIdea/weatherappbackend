﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using WeatherAppBackend.AppConfig;
using WeatherAppBackend.BackgroundServices;
using WeatherAppBackend.Services;

namespace WeatherAppBackend
{
    public class Program
    {
        static async Task Main(string[] args)
        {
            IHost host = new HostBuilder()
                .ConfigureAppConfiguration((hostContext, config) =>
                {
                    config.AddJsonFile("appsettings.json", optional: true, reloadOnChange: false);
                })
                .ConfigureServices((hostContext, services) =>
                {
                    services.AddHttpClient();
                    services.Configure<WeatherOptions>(hostContext.Configuration.GetSection("Weather"));
                    services.AddHostedService<WeatherBackgroundService>();
                    services.AddTransient<IWeatherService, WeatherService>();
                })
                .Build();

            await host.RunAsync();
        }
    }
}

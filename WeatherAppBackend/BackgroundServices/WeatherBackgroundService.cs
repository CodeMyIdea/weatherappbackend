﻿using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Options;
using System.Text.Json;
using WeatherAppBackend.AppConfig;
using WeatherAppBackend.Services;
using WeatherAppBackend.Services.Model;

namespace WeatherAppBackend.BackgroundServices
{
    public class WeatherBackgroundService : BackgroundService
    {
        private readonly IWeatherService _weatherService;
        private readonly WeatherOptions _weatherOptions;

        public WeatherBackgroundService(IWeatherService weatherService, IOptions<WeatherOptions> weatherOptions)
        {
            _weatherService = weatherService;
            _weatherOptions = weatherOptions.Value;
        }

        protected override async Task ExecuteAsync(CancellationToken cancellationToken)
        {
            while (!cancellationToken.IsCancellationRequested)
            {
                foreach (string country in _weatherOptions.Countries.Distinct())
                {
                    try
                    {
                        WeatherHistoryApiResponse weatherData = await _weatherService.GetWeatherHistory(country, cancellationToken);

                        string path = _weatherOptions.Path;
                        if (!Directory.Exists(path))
                        {
                            Directory.CreateDirectory(path);
                        }

                        string fileName = $"{path}/{country}.json";
                        if (File.Exists(fileName))
                        {
                            File.Delete(fileName);
                        }

                        File.WriteAllText(fileName, JsonSerializer.Serialize(weatherData, new JsonSerializerOptions { WriteIndented = true }));

                        Console.WriteLine($"Weather data for {country} has been fetched and saved to {fileName}");
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine($"An error occurred while fetching weather data for {country}: {ex.Message}");
                    }
                }

                await Task.Delay(TimeSpan.FromMinutes(_weatherOptions.FrequencyInMinutes), cancellationToken);
            }
        }
    }
}

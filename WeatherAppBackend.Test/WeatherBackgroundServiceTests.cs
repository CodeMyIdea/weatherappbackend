using Microsoft.Extensions.Options;
using Moq;
using System.Text.Json;
using WeatherAppBackend.AppConfig;
using WeatherAppBackend.BackgroundServices;
using WeatherAppBackend.Services;
using WeatherAppBackend.Services.Model;

namespace WeatherAppBackend.Test
{
    public class WeatherBackgroundServiceTests
    {
        [Fact]
        public async Task ExecuteAsync_Success()
        {
            // Arrange
            Mock<IWeatherService> weatherServiceMock = new();

            WeatherOptions weatherOptions = new WeatherOptions { FrequencyInMinutes = 60, Countries = new() { "USA", "United Kingdon" }, Path = "~/../../../../../weather-app-frontend/weather-data" };

            CancellationTokenSource cancellationTokenSource = new();
            CancellationToken cancellationToken = cancellationTokenSource.Token;

            WeatherHistoryApiResponse weatherDataUnitedKingdom = new WeatherHistoryApiResponse();
            WeatherHistoryApiResponse weatherDataUSA = new WeatherHistoryApiResponse();

            weatherServiceMock
                .SetupSequence(service => service.GetWeatherHistory(It.IsAny<string>(), It.IsAny<CancellationToken>()))
                .ReturnsAsync(weatherDataUnitedKingdom)
                .ReturnsAsync(weatherDataUSA);

            IOptions<WeatherOptions> weatherOptionsMock = Options.Create(weatherOptions);

            WeatherBackgroundService weatherBackgroundService = new WeatherBackgroundService(weatherServiceMock.Object, weatherOptionsMock);

            // Act
            await weatherBackgroundService.StartAsync(cancellationToken);
            await Task.Delay(TimeSpan.FromSeconds(1));
            cancellationTokenSource.Cancel(); // Cancel the background service

            // Assert
            foreach (string country in weatherOptions.Countries)
            {
                string filePath = $"{weatherOptions.Path}/{country}.json";
                Assert.True(File.Exists(filePath), $"File for {country} does not exist.");

                string fileContent = await File.ReadAllTextAsync(filePath);
                WeatherHistoryApiResponse weatherData = JsonSerializer.Deserialize<WeatherHistoryApiResponse>(fileContent);

                Assert.NotNull(weatherData);
            }
        }
    }
}
﻿using WeatherAppBackend.ExtensionFunctions;

namespace WeatherAppBackend.Test
{
    public class DateTimeFunctionsTests
    {
        [Fact]
        public void ConvertToCustomFormat_ValidDateTime_ReturnsFormattedString()
        {
            // Arrange
            DateTime dateTime = new DateTime(2022, 3, 14);
            string expected = "2022-03-14";

            // Act
            string result = dateTime.ConvertToCustomFormat();

            // Assert
            Assert.Equal(expected, result);
        }
    }
}

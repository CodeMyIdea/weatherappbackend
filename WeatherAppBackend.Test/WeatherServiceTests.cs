﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Options;
using Moq;
using WeatherAppBackend.AppConfig;
using WeatherAppBackend.Services;
using WeatherAppBackend.Services.Model;

namespace WeatherAppBackend.Test
{
    public class WeatherServiceTests
    {
        private readonly IConfiguration _configuration;

        public WeatherServiceTests()
        {
            IConfigurationBuilder configBuilder = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json", optional: false, reloadOnChange: true);
            _configuration = configBuilder.Build();
        }

        [Fact]
        public async Task GetWeatherHistory_Success()
        {
            // Arrange
            HttpClient httpClient = new Mock<HttpClient>().Object;
            WeatherOptions weatherOptions = _configuration.GetSection("Weather").Get<WeatherOptions>();
            IOptions<WeatherOptions> optionsMock = Options.Create(weatherOptions);

            string country = "USA";

            WeatherService service = new WeatherService(httpClient, optionsMock);

            // Act
            WeatherHistoryApiResponse result = await service.GetWeatherHistory(country, CancellationToken.None);

            // Assert
            Assert.NotNull(result);
        }
    }
}
